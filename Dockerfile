FROM node:16
WORKDIR /usr/src/app
COPY PrairieGrader ./
RUN npm install
CMD ["npm", "start"]
# HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD curl -f http://localhost:4000/ping || exit 1
